import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'index',
    component: () => import('../pages/Index.vue')
  },
  {
    path: '/dashboard',
    name: 'dashboard.index',
    component: () => import('../pages/Dashboard/Index.vue'),
  },
  {
    path: '/dashboard/user',
    name: 'dashboard.user',
    component: () => import('../pages/Dashboard/User/Index.vue'),
  },
  {
    path: '/dashboard/user/new',
    name: 'dashboard.user.new',
    component: () => import('../pages/Dashboard/User/New.vue'),
  },
  {
    path: '/dashboard/user/:user',
    name: 'dashboard.user.edit',
    component: () => import('../pages/Dashboard/User/Edit.vue'),
  },
  {
    path: '/dashboard/company',
    name: 'dashboard.company',
    component: () => import('../pages/Dashboard/Company/Index.vue'),
  },
  {
    path: '/dashboard/company/new',
    name: 'dashboard.company.new',
    component: () => import('../pages/Dashboard/Company/New.vue'),
  },
  {
    path: '/dashboard/company/:company',
    name: 'dashboard.company.edit',
    component: () => import('../pages/Dashboard/Company/Edit.vue'),
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
