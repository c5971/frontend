import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import style from './style.css'

import VueMask from '@devindex/vue-mask'; 

import moment from 'moment'

const app = createApp(App)


app.config.globalProperties.$filters = {
    dateBr(value) {
        let date = value
        return moment(date).format('DD/MM/YYYY')
    },
    maskCel(value) {
        return value
    },    
    maskCNPJ(value) {
        return value
    }
}

app
    .use(VueMask)
    .use(router)
    .mount('#app')